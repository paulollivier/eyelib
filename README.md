# eyelib

`eyelib` is the core and the shared functions for the EyeLib system. 

## Installation

In the project's `Cargo.toml`, use the following dependency:

```toml
# `branch` can also be sustituted with a particular `tag`.
eyelib = { git = "https://codeberg.org/paulollivier/eyelib.git", branch = "main" }
```
