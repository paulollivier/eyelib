//! Common stuff for the EyeSee (CI) Continuous Integration software.
//!
//! Contains some global definitions (such as [`Job`],
//! [`Executor`], some architecture documentation…).
//! ## The EyeSee architecture
//! EyeSee consists in the following components:
//! - A core: It recieves jobs, and dispatches them to available executors
//! - Executors: they recieve the job orders and execute them. They also provide
//!   information regarding how the execution went
//! - Input interfaces: some ways to schedule jobs:
//!   - github webhook (todo)
//!   - gitlab webhook (todo)
//!   - gitea webhook (todo)
//!   - repo-watcher (periodically checks a repo for changes) (todo)
#![deny(missing_docs)]
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(rustdoc::missing_doc_code_examples)]
#![deny(rustdoc::broken_intra_doc_links)]

use std::{fmt, fmt::Formatter, io::Error};

use chrono::{DateTime, Duration, Utc};
use serde::{Deserialize, Serialize};
use url::Url;
use uuid::Uuid;
use async_trait::async_trait;

/// This modules contains the serializable structures for communication between
/// the different parts of the EyeSee system.
pub mod net;

/// All stuffed that is linked to repositories, such as config
pub mod repositories;

/// An interface to be implemented by all executors
#[async_trait]
pub trait Executor {
    /// Called in order to execute a job.
    async fn execute(&self, job: Job) -> Result<JobResult>;
    /// Returns true if the executor can accept a new job
    async fn can_execute(&self) -> bool;
    /// Register the runner to the coordinator so it can be sent jobs. This
    /// implies the executor has a way of storing the coordinator URL and is
    /// capable of giving it's URL to the coordinator.
    async fn register(self, coordinator_url: Url) -> Result<()>;
    /// As with [`fn.register`], this methods signals the coordinator we are no
    /// longer to be sent jobs, and that this is normal
    async fn deregister(self, coordinator_url: Url) -> Result<()>;
}

/// Represents something that can have a duration
pub trait Durational {
    /// Returns the time elapsed during the implementor's execution
    fn duration(&self) -> Duration;
}

/// The result of an [`Executor`] operation
pub type Result<T> = std::result::Result<T, ExecutorError>;

/// Errors that could happen when the executor runs
#[derive(Debug, Serialize, Deserialize)]
pub enum ExecutorError {
    /// The Executor's configuration was invalid
    InvalidConfig,
    /// The [`Job`]'s configuration as invalid
    InvalidJob {
        /// A message documenting the error
        msg: String,
    },
    /// We encountered an error while running a specific [`Job`] step.
    StepError {
        /// Where we failed
        step: String,
        /// What has failed
        msg: String,
    },
    /// The [`JobWorkspace`] we got was invalid. Maybe the archive couldn't be
    /// decompressed, maybe the url was not resolved…
    InvalidWorkspace {
        /// A message documenting the error
        msg: String,
    },
    /// Represents an internal executor error.
    InternalError {
        /// A message documenting the error
        msg: String,
    },
}

impl fmt::Display for ExecutorError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ExecutorError::InvalidConfig => "Invalid executor config".to_string(),
                ExecutorError::InvalidJob { msg } => format!("Invalid job: {}", msg),
                ExecutorError::StepError { step, msg } =>
                    format!("Step error while executing step \"{}\": {}", step, msg),
                ExecutorError::InvalidWorkspace { msg } =>
                    format!("Invalid workspace given: {}", msg),
                ExecutorError::InternalError { msg } => {
                    format!("Internal Executor error: {}", msg)
                }
            }
        )
    }
}

impl From<Error> for ExecutorError {
    fn from(_: Error) -> Self {
        todo!()
    }
}

/// Something to run
#[derive(Debug, Serialize, Deserialize)]
pub struct Job {
    /// An arbitrary name given to the job
    pub job_name: String,
    /// An execution ID, used to match a [`JobResult`] with its
    /// origin [`Job`].
    pub id: Uuid,
    /// The docker image this job should run in
    pub image: String,
    /// List of shell commands to be called inside the container
    pub steps: Vec<String>,
    /// Where should we get the data to populate the working directory?
    pub workspace: JobWorkspace,
}

/// Represent the various types of workspace
#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum JobWorkspace {
    /// `git clone` the workspace from the given URL
    Git(Url),
    /// get the workspace from the given URL and decompress it using targz
    TarGz(Url),
    // TODO: implement more types
}

/// What happened during run
#[derive(Debug, Serialize, Deserialize)]
pub struct JobResult {
    /// An execution ID, used to match a [`JobResult`] with its
    /// origin [`Job`].
    pub id: Uuid,
    /// All the execution data of the various steps
    pub steps_results: Vec<StepResult>,
    /// Resulting workspace
    pub workspace: JobWorkspace,
    /// When this job was started
    pub started_at: DateTime<Utc>,
    /// When this job was finished
    pub finished_at: DateTime<Utc>,
}

impl From<&Job> for JobResult {
    fn from(job: &Job) -> Self {
        Self {
            id: job.id,
            steps_results: vec![],
            workspace: job.workspace.clone(),
            started_at: Default::default(),
            finished_at: Default::default(),
        }
    }
}

impl Durational for JobResult {
    fn duration(&self) -> Duration {
        self.finished_at - self.started_at
    }
}

/// Result of a step execution
#[derive(Debug, Serialize, Deserialize)]
pub struct StepResult {
    /// Command that has been executed
    pub step: String,
    /// List of dated stdout lines this step has producted
    pub logs: Vec<(DateTime<Utc>, String)>,
    /// When this step was started
    pub started_at: DateTime<Utc>,
    /// When this step was finished
    pub finished_at: DateTime<Utc>,
}

impl Durational for StepResult {
    fn duration(&self) -> Duration {
        self.finished_at - self.started_at
    }
}
