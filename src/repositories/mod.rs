use serde::Deserialize;

/// All the different revision to the config file format. See variant's
/// documentation for examples. matches the `version` part of the conig
#[derive(Deserialize)]
#[serde(rename_all = "lowercase", tag = "version")]
pub enum RepositoryConfig {
    /// Revision one of the config
    R1(r1::RepositoryConfig),
}

#[cfg(test)]
mod tests {
    use crate::repositories::RepositoryConfig;

    #[test]
    #[allow(unreachable_patterns)]
    fn test_deserialize_r1() {
        let config = r#"---
version: r1
jobs:
  - image: busybox:latest
    steps:
      - echo "hello"
"#;
        let repo_config: RepositoryConfig = serde_yaml::from_str(config).unwrap();
        match repo_config {
            RepositoryConfig::R1(config) => {
                assert_eq!(config.jobs.len(), 1);
            }
            _ => panic!("invalid variant"),
        }
    }
}

/// Revision 1: the draft of a config file specification. See module
/// documentation for more details
///
/// # Example configuration file
/// ```yaml
/// # A version number identifying the version of the config to use
/// version: r1
/// # The `jobs` map describes different jobs. They _may_ be independent in the future
/// jobs:
///   - name: build
///     image: rust:1.55
///     steps:
///       - cargo build
///       - cargo test
///       - cargo doc
///   # the workspace of the `build` job is available in this next one
///   - name: publish doc
///     image: rust:1.55
///     steps:
///       - scp target/doc/ user@my-server:/var/www/mycrate/
/// ```
pub mod r1 {
    use serde::Deserialize;
    /// Root configuration structure
    #[derive(Deserialize)]
    pub struct RepositoryConfig {
        /// a list of jobs to be run against this repository
        pub jobs: Vec<RepositoryConfigJob>,
    }

    /// A job configuration
    #[derive(Deserialize)]
    pub struct RepositoryConfigJob {
        /// which docker image should we use to run our stuff
        pub image: String,
        /// list of 'steps' (shell commands) to be run inside a container
        /// created from the image
        pub steps: Vec<String>,
    }
}
